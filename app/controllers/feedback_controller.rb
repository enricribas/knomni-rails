class FeedbackController < ApplicationController

  def create
    save_feedback
    send_email
    render json: resp
  end

  private

  def save_feedback
    Feedback.create!(
      user_id: params["user"]["id"],
      email: params["user"]["email"],
      comment: params["text"]
    )
  end

  def send_email
  end

  def resp
    {
      feedback: "accepted",
      body: comment
    }
  end

  def comment
    params["text"]
  end
end
