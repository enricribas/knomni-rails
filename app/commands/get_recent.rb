module Commands
  class GetRecent < RailsEventStore::Event
    def response
      taggings_serializer(taggings)
    end

    private

    def taggings
      Tagging.includes(:tag).order("id").last(25).reverse
        .uniq {|t| t.tag.text}
    end

    def taggings_serializer(taggings)
      taggings.map {|t|
        {
          tag: t.tag.text,
          websites: []
        }
      }
    end

    def websites_serializer(website)
      [WebsitePresenter.new(website).as_json]
    end
  end
end
