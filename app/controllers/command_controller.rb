class CommandController < ApplicationController

  # XXX I'm mixing up commands and events. Will it matter?
  COMMANDS = {
    'TAG_WEBSITE' => ::Commands::TagWebsite,
    'SET_URL'     => ::Commands::SetUrl,
    'SEARCH_TAGS' => ::Commands::SearchTags,
    'CLEAR_TAGS'  => ::Commands::ClearTags,
    'SET_USER'    => ::Commands::SetUser,
    'GET_TAGS'    => ::Commands::GetTags,
    'GET_RECENT'  => ::Commands::GetRecent
  }

  def create
    response = publish( command_runner.new( data: event_params ))

    render json: response
  end

  private

  def event_params
    {
      user:    params["user"],
      payload: params["payload"]
    }
  end

  def command_runner
    COMMANDS.fetch params[:command]
  end

  def publish(event)
    EventStore.event_store.publish_event event

    event.response.to_json
  end
end
