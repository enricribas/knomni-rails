class Tag < ApplicationRecord
  has_many :taggings
  has_many :websites, through: :taggings
end
