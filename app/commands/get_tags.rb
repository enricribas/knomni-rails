module Commands
  class GetTags < RailsEventStore::Event
    def response
      tag_serializer(tags)
    end

    private

    def tags
      Tag.all.sort
    end

    def tag_serializer(tags)
      tags.map {|t|
        {
          tag: t.tag.text,
        }
      }
    end
  end
end
