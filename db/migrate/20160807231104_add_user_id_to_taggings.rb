class AddUserIdToTaggings < ActiveRecord::Migration[5.0]
  def change
    add_column :taggings, :user_id, :string
  end
end
