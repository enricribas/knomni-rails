class AddTitleToWebsite < ActiveRecord::Migration[5.0]
  def change
    add_column :websites, :title, :string
  end
end
