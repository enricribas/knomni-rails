# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161002230525) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "event_store_events", force: :cascade do |t|
    t.string   "stream",     null: false
    t.string   "event_type", null: false
    t.string   "event_id",   null: false
    t.text     "metadata"
    t.text     "data",       null: false
    t.datetime "created_at", null: false
    t.index ["created_at"], name: "index_event_store_events_on_created_at", using: :btree
    t.index ["event_id"], name: "index_event_store_events_on_event_id", unique: true, using: :btree
    t.index ["event_type"], name: "index_event_store_events_on_event_type", using: :btree
    t.index ["stream"], name: "index_event_store_events_on_stream", using: :btree
  end

  create_table "feedbacks", force: :cascade do |t|
    t.string   "user_id"
    t.string   "email"
    t.text     "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.integer  "website_id"
    t.float    "strength",   default: 0.0
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "user_id"
    t.index ["tag_id"], name: "index_taggings_on_tag_id", using: :btree
    t.index ["website_id"], name: "index_taggings_on_website_id", using: :btree
  end

  create_table "tags", force: :cascade do |t|
    t.string   "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "websites", force: :cascade do |t|
    t.string   "url"
    t.string   "root_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "title"
  end

end
