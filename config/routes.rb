Rails.application.routes.draw do
  root 'status#index'

  scope :api do
    get  'tags'             => "status#tags"
    post 'command/:command' => "command#create"
    post 'feedback'         => "feedback#create"
  end
end
