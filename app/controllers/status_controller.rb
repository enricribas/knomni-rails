class StatusController < ApplicationController
  def index
    render json: { status: true }
  end

  def tags
    render json: { tags: tag_count }
  end

  private

  def tag_count
    # FIXME count in SQL not memory

    website = Website.find_by(url: params["url"])
    website ? website.tags.count : 0
  end
end
