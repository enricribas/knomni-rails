class TagPresenter
  attr_accessor :tag
  delegate :text, to: :tag

  def initialize(tag)
    @tag = tag
  end

  def as_json(*)
    {
      text: text
    }
  end
end
