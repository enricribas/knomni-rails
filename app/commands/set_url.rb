module Commands
  class SetUrl < RailsEventStore::Event
    def response
      {
        url: url, 
        tags: tags,
        recent: websites
      }
    end

    private

    def tags
      website = Website.find_by(url: url)

      if website
        website.tags.map {|t| { text: t.text }}
      else
        []
      end
    end

    def url
      data.payload["website"]["url"]
    end

    # FIXME This should return most recently
    #       tagged by current_user but for now
    #       we have no auth yet
    def websites
      websites_serializer(Website
        .includes(:tags)
        .last(5)
        .to_a
      )
    end

    def websites_serializer(websites)
      websites.map {|w|
        WebsitePresenter.new(w).as_json
      }
    end
  end
end
