# FU Rails this doesn't get loaded in Production
require_relative '../../app/listeners/taggings'

module EventStore
  def self.event_store
    @event_store ||= RailsEventStore::Client.new.tap do |es|
      es.subscribe(Listeners::Taggings.new, [Commands::TagWebsite])
    end
  end
end
