module Commands
  class SearchTags < RailsEventStore::Event
    def response
      { 
        websites: websites_serializer(websites),
        tags: tags
      }
    end

    private

    def tags
      # FIXME n+1
      tags = websites.flat_map {|w| w.tags}.map {|t| t.text}.uniq
      tags - search_tags
    end
    
    def websites
      @websites ||= begin
        query = <<-SQL
          SELECT websites.* FROM websites WHERE EXISTS (
            SELECT  NULL
            FROM taggings tg
            JOIN tags t ON t.id = tg.tag_id
            WHERE t.text IN (:tags)
            AND tg.website_id = websites.id
            GROUP BY tg.website_id
            HAVING COUNT(DISTINCT t.text) = :count
          )
          LIMIT 25;
        SQL

        Website.find_by_sql([query, search_params])
      end
    end

    def search_params
      {
        tags: search_tags,
        count: search_tags.count
      }
    end

    def search_tags
      data.payload["text"]
    end
      
    def websites_serializer(websites)
      websites.map {|w|
        WebsitePresenter.new(w).as_json
      }
    end
  end
end
