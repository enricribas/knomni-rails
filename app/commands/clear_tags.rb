module Commands
  class ClearTags < RailsEventStore::Event
    def response
      {}
    end
  end
end
