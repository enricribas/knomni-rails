class CreateTaggings < ActiveRecord::Migration[5.0]
  def change
    create_table :taggings do |t|
      t.references :tag
      t.references :website
      t.float :strength, default: 0

      t.timestamps
    end
  end
end
