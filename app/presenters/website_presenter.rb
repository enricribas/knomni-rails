class WebsitePresenter
  attr_accessor :website
  delegate :url, :title, to: :website

  def initialize(website)
    @website = website
  end

  def as_json(*)
    {
      title: title,
      url: url,
      tags: tags
    }
  end

  private

  def tags
    []
    # TODO Add tags for each website one day
    # website.tags.map {|t|
    #   TagPresenter.new(t).as_json
    # }
  end
end
