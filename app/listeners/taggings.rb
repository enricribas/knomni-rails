module Listeners
  class Taggings
    def call(event)
      payload = event.data.payload
      url     = payload["website"]["url"]

      website = Website.where(url: url).first_or_initialize do |w|
        w.title = payload["website"]["title"]
      end

      tag = Tag.where(text: payload["tag"]).first_or_initialize

      website.save!
      tag.save!

      Tagging.create!(
        user_id: event.data.user["id"],
        tag: tag,
        website: website
      )
    end
  end
end
