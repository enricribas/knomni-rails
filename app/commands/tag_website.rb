module Commands
  class TagWebsite < RailsEventStore::Event
    def response
      {
        id:  data.payload["id"],
        tag: data.payload["tag"],
        url: website_data["url"]
      }
    end

    private

    def website_data
      data.payload["website"]
    end
  end
end
