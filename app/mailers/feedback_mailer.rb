class FeedbackMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.feedback_mailer.send_email.subject
  #
  def send_email
    @greeting = "Hi"

    mail to: "to@example.org"
  end
end
